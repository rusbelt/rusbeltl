<?php
/** 
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //
/** El nombre de tu base de datos de WordPress */
define('DB_NAME', 'negocio');

/** Tu nombre de usuario de MySQL */
define('DB_USER', 'root');

/** Tu contraseña de MySQL */
define('DB_PASSWORD', '');

/** Host de MySQL (es muy probable que no necesites cambiarlo) */
define('DB_HOST', 'localhost');

/** Codificación de caracteres para la base de datos. */
define('DB_CHARSET', 'utf8mb4');

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', '@a=}w:yuy]c_/5sIG-eGUs(Vfi&UJ9(cIuNr/9YTd|lPBwY=j?2r^2M41&VeX;:Z');
define('SECURE_AUTH_KEY', 'NrV}}iETKE5u{zInc;B#RoAx?Xmv^CY/(td_|6nbFNgo/Qu=V:V=.mxEnq%OPM*C');
define('LOGGED_IN_KEY', 'uBjo<zmlS0MXa~/O(4I6d{M.5}haU^zh4p2dRv3piH:sOTFu(L3LE>yxJ(_r5,FD');
define('NONCE_KEY', 'I)wavX|N+Kf=pzrqnu5Zr_pP$]w3Z2j3< cWdn 1-Tf&uE7GN 4iH bF+rX(}Du7');
define('AUTH_SALT', '~r:U}yKi:oVwsNrpX}!FsY^$.vY,sRl22/s[Ifj1:Nk7UH+Y4(+nuY%t~W[s#)RN');
define('SECURE_AUTH_SALT', 'S{KCo0<B#i&P%C^]@nVNE|th?b[zr.uCAVe8{M6AoVkT(b|]K$//hl@_0=uYy|Y5');
define('LOGGED_IN_SALT', '>77D>@hxr=_RG!9Qd:p)s@;V.=9B7`RJ#1 5(>qdy0${;MK(s-`yd`?_.HF)=K] ');
define('NONCE_SALT', '{k>*v:g8QMk#`z;{mOm#klPC%u%8mR:p`s9=ql2,QQ]qz[E53T6m?R,aL3]dXB}w');

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix  = 'wp_negocio';


/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', false);

/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

